import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:form_login/home.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool _showPass = false;
  TextEditingController _userController = new TextEditingController();
  TextEditingController _passController = new TextEditingController();
  var _userErr = "Tài khoản không hợp lệ";
  var _passErr = "Mật khẩu không hợp lệ";
  var _userValid = false;
  var _passValid = false;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Container(
          padding: EdgeInsets.fromLTRB(30, 30, 30, 20),
          constraints: BoxConstraints.expand(),
          color: Colors.white,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 40.0),
                child: Container(
                    width: 70,
                    height: 70,
                    padding: EdgeInsets.all(10),
                    // color: Colors.grey,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: Colors.blueGrey),
                    child: FlutterLogo()),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 80.0),
                child: Text(
                  "Hello \nWelcome Back",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 40),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 20),
                child: TextField(
                  controller: _userController,
                  style: TextStyle(fontSize: 25),
                  decoration: InputDecoration(
                    labelText: "USERNAME",
                    errorText:_userValid?_userErr:null,
                    labelStyle: TextStyle(
                      color: Colors.grey,
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 20),
                child:
                    Stack(alignment: AlignmentDirectional.centerEnd, children: [
                  TextField(
                    controller: _passController,
                    obscureText: !_showPass,
                    style: TextStyle(fontSize: 25),
                    decoration: InputDecoration(
                      labelText: "PASSWORD",
                      errorText: _passValid?_passErr:null,
                      labelStyle: TextStyle(
                        color: Colors.grey,
                        fontSize: 20,
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: onToggleShowhide,
                    child: Text(
                      _showPass ? "HIDE" : "SHOW",
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.blueAccent),
                    ),
                  )
                ]),
              ),
              SizedBox(
                  width: double.maxFinite,
                  height: 46,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8.0))),
                    color: Colors.blue,
                    onPressed: onSignin,
                    child: Text(
                      "Sign In",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 17),
                    ),
                  )),
              Container(
                height: 110,
                width: double.infinity,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "New User ? SIGNUP",
                      style: TextStyle(color: Colors.grey),
                    ),
                    Text(
                      "FORGOT PASSWORD",
                      style: TextStyle(color: Colors.blue),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void onToggleShowhide() {
    setState(() {
      _showPass = !_showPass;
    });
  }
  void onSignin(){
    setState(() {
      if(_userController.text.length<6||!_userController.text.contains("@")){
                _userValid=true;
      }
      else {
        _userValid = false;
      }
      if(_passController.text.length<6){
        _passValid=true;
      }
      else {
        _passValid = false;
      }
      if (!_passValid && !_userValid){
        // Navigator.push(context, MaterialPageRoute(builder: gotoHome));
        Navigator.push(context,MaterialPageRoute(builder: (context)=>HomePage()));
      }
    });
  }
  // Widget  gotoHome (BuildContext context){
  //   return HomePage();
  // }
}
